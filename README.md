# Docker

## 1.krok: Inštalácia

https://docs.docker.com/engine/installation/

## 2.krok: Úpravy nastavení po inštalácií základného dockeru:
### bin/post-installation
#### UPDATE 2017-02-28
nastavenie .env - treba nastaviť cesty k projektom, k databáze a ssh klúčom
spustenie bin/install


## 3.krok:

* vytvorit index.php do rootu priecinku, odkial sa budu tahat projekty --> zapisat do neho phpinfo(); (localhost.conf) (URL localhost)
    - vytvorit podpriecinok adminer, v ktorom bude index.php zo stranky https://www.adminer.org/ (adminer.conf) (URL adminer.dev)
    - stiahnut z gitu artemis-project(pri klonovani nezadavat custom cestu, aby sa priecinok volal artemis-project a pod nim bol priecinok web, ktory obsahuje index.php) (artemis.conf) (URL artemis.sme.localhost)
    - je potrebne vytvorit podpriecinky smedata a usmedata, ktore budu obsahovat podpriecinok artemis (smedata.conf, usmedata.conf)
    - OPTIONAL - daju sa odkomentovat sluby,phpinfo,storm
        - storm je pripraveny na zakladny prazdny storm project
        - phpinfo je pripravene na priecinok phpinfo, ktory by obsahoval subor index.php v ktorom bude zapisane "<?php phpinfo(); ?>"
        - sluby su pripravene na sluby-project, ktory sa da stiahnut z gitu, staci odkomentovat riadok so sluby.conf v images/dev/proxy/Dockerfile a v post-installation 3 riadky tykajuce sa slubov


/*********************************************************************************************/

## 4.krok - priečinok bin (OPTIONAL)
- pre spustenie stačí v terminály spustiť **up**
- pre vypnutie a zmazanie všetkých kontainerov stačí spustiť **clean-containers**
- pre zmazanie všetkých imageov, ktoré si docker vytvoril stačí spustiť **clean-images**
- pre rýchle vypnutie aktuálneho lokálneho apache,mysql,memcached serveru(kvôli konfiktom) stačí spustiť **apache-stop**
- pre rýchle zapnutie aktuálneho lokálneho apache,mysql,memcached serveru(kvôli konfiktom) stačí spustiť **apache-start**
- pre získanie informácií o dockeri stačí spustiť **info**
- pre pripojenie sa cez ssh do docker kontaineru stačí spustiť **ssh &lt;nazov kontajneru&gt;** 
- pre zistenie stavu jednotlivých kontajnerov stačí spustiť **status**
- pre zastavenie kontajnerov stačí spustiť **stop**
- pre spustenie kontajnerov stačí spustiť **start**
- pre skopírovanie ssh kľučov a spustenie composer install na daný(é) projekt(y) pod aktuálnym lokálnym užívateľom stačí spustiť **post-installation** (spúšťa sa v **up** (počíta sa s tým, že bol správne zadefinovaný užívateľ v kontajneri v images/dev/web/Dockerfile  s názvom **docker_php_1**))
- pre vymazanie nevyužitých docker images kvôli uvoľneniu miesta stačí spustiť **clean-unused-images**
- pre spustenie xdebugu v dockeri stačí spustiť **xdebug-start**
- pre vypnutie xdebugu v dockeri stačí spustiť **xdebug-stop**

## INFO:

* images/dev/web/Dockerfile -> instalacia php
    - obsahuje: php s rozsireniami, git, composer, zip(nepovinny zatial), dependency baliky...

## Nastavenia
* mysql_host: mysql
* mysql_user: root
* mysql_pass: admin  (da sa zmenit v docker-compose.yml MYSQL_ROOT_PASSWORD)

## Vytvorenie SSL certifikatov:
(missing info)
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt
